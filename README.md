SuperTux Megacontest
====================

There are N computers in the room ordered in a circle.
The computers are running a different SuperTux versions with a set of M levels.
Each player has a home computer, which is one of these N computers.
Each player starts at her/his home computer.
When a player finishes the level, s/he goes to the next computer in the circle.
If that computer is occupied, s/he skips the computer and goes to the next one until s/he arrives at a free computer.
And when the player gets to her/his home computer, s/he plays the next level.
The first player who finishes all M levels wins.

This contest is in a virtual room, so each player plays the contest on her/his own personal computer.
And there is a python script which lets the player play the correct level in the correct SuperTux version.
However, it takes a bit longer to configure.

You can watch the virtual room at: 37.46.208.34:30001 (when the constest is on)

*The contest is scheduled to 31th June 16:00 UTC. Please, join SuperTux Discord at least 15 minutes in advance.*

Before you join the contest, make sure that everything is configured correctly.
You will be able to test your configuration before the actual contest starts.

How to configure this
=====================

You need to install and configure all these SuperTuxes:

Your Profile
------------
* Go to `config.py` and scroll to the end. Set the variable `nick` to the nick you want to use.
* On the line right bellow, set the variable `img` to the URL of the profile image you want to use. It must be uploaded somewhere on the Internet where anybody can view it.

SuperTux 0.6.3
--------------
* Download and install SuperTux 0.6.3
* Go to `config.py` and set the value `exec063` to the executable and `data063` to the data directory.
* Download The Crystal Catacombs from: https://raw.githubusercontent.com/SuperTux/addons/92772fff2a7172afd70d0053f49be15225164cb5/repository/the-crystal-catacombs.zip
* ... and unpack it into the data directory.
* Download Treasure Mountain from: https://raw.githubusercontent.com/SuperTux/addons/25dbd9f7feded11079205b562360c3727915a5ad/repository/treasure-mountain.zip
* ... and unpack it into the data directory. Do not overwrite any files.

SuperTux 0.5.1
--------------
* Download and install SuperTux 0.5.1
* Go to `config.py` and set the value `exec051` to the executable and `data051` to the data directory.
* Download Yeti's Revenge from: https://github.com/SuperTux/addons/raw/935e9527ec9bfa3999a7cb08e20380bcba43ac93/repository/niso-yetis-revenge_v3.zip
* ... and unpack it into the data directory. Overwrite all existing files.
* Download Mattie's Iceberg from: https://github.com/SuperTux/addons/raw/38a8b62f52759f2c1f6ce7e9504257e18fc07ac9/repository/lmh-matties-world_v6.zip
* ... and unpack it into the data directory. Overwrite all existing files.
* Download Overunderground from: https://gitlab.com/cecflit/overunderground/-/archive/master/overunderground-master.zip
* ... and unpack it into the directory **data/levels**. Rename the extracted directory to `overunderground`.

SuperTux 0.1.4
--------------
* Download and install SuperTux 0.1.4
* Go to `config.py`. In the table `tuxes` → `014` set the values `exec` to the executable and `data` to the data directory
* If you have a problem with water transparency, try turning off OpenGL.

SuperTux Extra
--------------
* Create another copy of SuperTux 0.1.4
* Replace the `data` directory with: https://www.dropbox.com/s/n5i46nn21cvjw61/stex2_data.tar.bz2?dl=0
* Go to `config.py`. In the table `tuxes` → `extra` set the values `exec` to the executable and `data` to the data directory
* If you have a problem with water transparency, try turning off OpenGL.

TeraTux
-------
* Create another copy of SuperTux 0.1.4
* Replace the `data` directory with: https://www.dropbox.com/s/xwbw3cgwhhu0a62/teratux_data.zip?dl=0
* Go to `config.py`. In the table `tuxes` → `teratux` set the values `exec` to the executable and `data` to the data directory
* If you have a problem with water transparency, try turning off OpenGL.

SuperTux 0.0.4a
---------------
* Download source from: https://cdn.discordapp.com/attachments/396022213100437504/1000106379048128653/supertux-0.0.4a.tar.gz
* If you cannot build the source, download the version **0.0.4a** from: https://github.com/supertux-community/supertux-binaries/releases/tag/0.0.4
* Download this levelset: https://www.dropbox.com/scl/fo/ljwboa7b2nx3e22jeluir/h?dl=0&rlkey=misvdq1a43y8bsqpi2plc7nio
* ...and extract it to the folder **data/level_bank**
* Go to `config.py`. In the table `tuxes` → `004` set the values `exec` to the executable, `level` to the path to the file `data/levels/level1.dat` and `level_bank` to the path to the folder `data/level_bank`.

After you finish this
=====================
When you want to join the contest, just write `python3 megacontest.py`. Do not run this script before you are told to do so.
If you come too late, you can still join at any time, you will be just penalised by the amount of time you were late.
If your computer gets stuck and you need to reboot, just run the script again and you will continue where you ended.
