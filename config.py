world1 = ["world1/level%d.stl"%(i) for i in range(1, 27)]
l004 = ["level%d.dat"%(i) for i in range(1, 11)]
narre = ["Narre/evel11.stl", "Narre/evel27.stl", "Narre/evel15.stl", "Narre/evel12.stl", "Narre/evel29.stl", "ver2/rainforest.stl", "Narre/evel0.stl", "ver2/wayhole.stl", "narrenew/pokus.stl", "narrenew/level8.stl", "narrenew/level9.stl", "narrenew/level11.stl", "narrenew/level12.stl", "narrenew/level13.stl", "ver2/bewarelava.stl", "ver2/lavafalls.stl", "ver2/snowwater.stl", "ver2/fishes.stl", "narrenew/level7.stl", "narrenew/level4.stl", "narrenew/level6.stl", "ver2/lavacastle.stl", "ver2/greatfall.stl", "ver2/meltingblocks.stl", "ver2/lavalake.stl", "ver2/classiclava.stl", "ver2/volcano.stl", "ver2/burnbridge.stl", "ver2/rainlava.stl", "ver2/lavafortress.stl", "ver2/oceanagain.stl", "ver2/classicocean.stl", "ver2/welcomebadguy.stl", "ver2/welcomegum.stl", "ver2/gumissticky.stl", "ver2/thunderstorm.stl", "ver2/classicgum.stl", "ver2/morelives.stl", "ver2/gummountains.stl", "ver2/fallunderground.stl", "ver2/gumcave.stl", "ver2/caveescape.stl", "ver2/entercastle.stl", "ver2/stickycastle.stl", "ver2/classicantarctica.stl", "ver2/snowplains.stl", "ver2/narrebridge.stl"]

def ttl(i):
	m = int((i-1)/100)*100
	return "teratux/%d-%d/level%d.stl"%(m+1, m+100, i-m)
teratux = [ttl(i) for i in [25, 37, 48, 66, 101, 201, 202, 203, 205, 301, 401, 406, 501, 601, 603, 655, 708, 711, 721, 781, 901, 903, 954, 1000]]

w1_063 = ["world1/"+s for s in ["above_arctic_skies.stl", "between_glaciers.stl", "castle_of_nolok.stl", "crystal_mine.stl", "end_of_tunnel.stl", "entrance_cave.stl", "fork_in_the_road.stl", "frosted_fields.stl", "frozen_bridge.stl", "ice_in_the_hole.stl", "icy_valley.stl", "into_stars.stl", "journey_begins.stl", "living_inside_fridge.stl", "miyamoto_monument.stl", "more_snowballs.stl", "night_chill.stl", "or_just_me.stl", "path_in_the_clouds.stl", "shattered_bridge.stl", "somewhat_smaller_bath.stl", "stone_cold.stl", "under_the_ice.stl", "via_nostalgica.stl", "welcome_antarctica.stl", "23rd_airborne.stl"]]
w2_063 = ["world2/"+s for s in ["ancient_ruins.stl", "beside_bushes.stl", "bouncy_coils.stl", "bye_bye_forest.stl", "crumbling_path.stl", "darkness_awaits.stl", "find_big_fish.stl", "ghostly_misery.stl", "ghouls_lair.stl", "going_underground.stl", "i_spy_with_my_little_eye.stl", "leaf_wind.stl", "lost_village.stl", "mouldy_grotto.stl", "owls_again.stl", "owls_skydive_commando.stl", "penguin_grow_trees.stl", "shocking.stl", "the_forest_is_rotting.stl", "through_dark.stl", "tower_of_ghosts.stl", "tux_builder.stl", "tux_own_horror_show.stl", "walking_leaves.stl", "welcome_forest.stl", "wooden_roots.stl"]]

w1_051 = ["world1/"+s for s in ["01 - Welcome to Antarctica.stl", "02 - The Journey Begins.stl", "03 - Via Nostalgica.stl", "04 - Tobgle Road.stl", "05 - The Somewhat Smaller Bath.stl", "06 - The Frosted Fields.stl", "07 - Oh No More Snowballs.stl", "08 - Stone Cold.stl", "09 - Grumbels Sense of Snow.stl", "10 - 23rd Airborne.stl", "11 - Night Chill.stl", "12 - Into the Stars.stl", "13 - Above the Arctic Skies.stl", "14 - Entrance to the Cave.stl", "15 - Under the Ice.stl", "16 - Living in a Fridge.stl", "17 - Or is it just me.stl", "18 - Ice in the Hole.stl", "19 - Miyamoto Monument.stl", "20 - End of the Tunnel.stl", "21 - A Path in the Clouds.stl", "22 - A Mysterious House of Ice.stl", "23 - The Escape.stl", "24 - The Shattered Bridge.stl", "25 - Arctic Ruins.stl", "26 - The Castle of Nolok.stl"]]
w2_051 = ["world2/"+s for s in ["Bouncy_Coils.stl", "CounterCurrent.stl", "Crumbling_Path.stl", "dan_morial.stl", "detour.stl", "Duct_Ape.stl", "El_Castillo.stl", "Find_Big_Fish.stl", "ghostly.stl", "Going_Underground.stl", "light+magic.stl", "Little_Venice.stl", "Mouldy_Grotto.stl", "Penguin_Grow_Trees.stl", "Silent_Walls.stl", "Three_Sheets.stl", "Tux_Builder.stl", "Two_Towers.stl", "Up_and_Down.stl", "village.stl", "Walking_Leaves.stl", "Welcome_Forest.stl"
]]
yeti = ["yetis_revenge/"+s for s in ["A Bombastic Day.stl", "Above The Clouds.stl", "A Wonderful Day.stl", "Big Wave.stl", "Blue Mountain.stl", "Carry Me.stl", "Cave Of Waterfalls.stl", "Controlling the Temperature.stl", "Crazy Cave.stl", "Dont Be Afraid.stl", "Escape From The Pirates.stl", "Foreign Passenger.stl", "Freeze n Flee.stl", "Hail Storm.stl", "Hang Out In The Underground.stl", "I Go With My Latern.stl", "Its Getting Colder.stl", "Leaving Home And Start The Journey.stl", "Living Spikes.stl", "Lotus Grotto.stl", "More Of The Blue Mountain.stl", "Next To The Stars.stl", "No Bridge Out There.stl", "Path Without Name.stl", "Pirates Harbor.stl", "Playground In The Water.stl", "Reaching The Top.stl", "Shaft Racing.stl", "Slippery Climbing.stl", "Something In The Water.stl", "Something To Enjoy.stl", "Spooky Way Of Life.stl", "Staple Me.stl", "Still Waters Run Deep.stl", "Surfing Tux.stl", "The Grand Bridge.stl", "The Way Down.stl", "Through The Groundwater.stl", "Windy Day On Ice.stl"]]
mattie = ["mattie_world/mattie_iceberg/"+s for s in ["L_Cavern.stl", "L_DOOM.stl", "L_Dungeon.stl", "L_Escape.stl", "L_Fields.stl", "L_HighRoute.stl", "L_Meltbox.stl", "L_Mineshaft.stl", "L_Moat.stl", "L_MtnPass.stl", "L_Quarry.stl", "L_ScarySkies.stl", "L_ThreeLanterns.stl", "L_Wall.stl"]]
oug = ["overunderground/center%d.stl"%(i) for i in range(1,5)] + ["overunderground/forest%d.stl"%(i) for i in [*range(1,7), *range(8,17)]] + ["overunderground/jungle%d.stl"%(i) for i in [*range(1,13), *range(14,17)]] + ["overunderground/sky%d.stl"%(i) for i in [*range(1,13), *range(14,17)]] + ["overunderground/underground%d.stl"%(i) for i in [*range(1,11), *range(12,17)]]
tcc = ["The_Crystal_Catacombs/"+s for s in ["1_The_Journey_Begins_Again.stl", "10_Beside_The_Cold_Waters.stl", "11_Cruel_Cold_Waters.stl", "12_Defibrillation.stl", "13_Permafrost.stl", "14_The_Watchtower.stl", "15_Diving_into_the_Depths.stl", "16_Entrance_to_the_Catacombs.stl", "17_The_Supply_Site.stl", "18_Tux_the_Illuminated_Acrobat.stl", "19_Ventilation_Adventure.stl", "2_Climbing_and_Swinging.stl", "20_Tux_the_Photomaniac.stl", "21_A_Light_in_the_Darkness.stl", "22_The_Mineshaft.stl", "23_From_Pyre_to_Pyre.stl", "24_Disco_of_the_Dead.stl", "25_Between_a_Rock_and_a_Hard_Place.stl", "26_Light_and_Magic.stl", "27_Rising_and_Falling.stl", "28_The_Gravity_of_the_Situation.stl", "29_The_Pit_of_the_Lost_Souls.stl", "3_A_Hamlet_Named_Quartz.stl", "30_The_Injured_Bridge.stl", "31_Kicking_Up_a_Fuss.stl", "32_Stepping_Stones.stl", "33_Blind_to_the_World.stl", "34_Hanging_by_a_Thread.stl", "35_Rusted_and_Decayed.stl", "36_Two_Tricky_Tasks.stl", "37_Countercurrent.stl", "38_A_Crushing_Blow.stl", "39_Purgatory.stl", "4_A_Parting_Through_the_Arctic_Forest.stl", "40_The_Light_at_the_End_of_the_Tunnel.stl", "41_Stairway_to_Heaven.stl", "42_The_Meaning_of_Life.stl", "5_The_Battleground.stl", "6_Fjord_of_Fortitude.stl", "7_Bumpy_High_Mountain_Skies.stl", "8_The_Bountiful_Summit.stl", "9_Wallkicks_Will_Work.stl"]]
treasure = ["Treasure_Mountain/"+s for s in ["A_Clear_Day.stl", "After_Rains.stl", "A_Village_In_The_Hills.stl", "A_World_Turned_Snowy.stl", "Dark_And_Damp.stl End.stl", "Foggy_Island.stl", "Heavy_Downpour.stl", "Into_The_Shaft.stl", "Into_The_Wild.stl", "Mountainous_Winds.stl", "Night_Of_The_Living_Pumpkins.stl", "Pumpkin_Farm.stl", "The_Forest_Is_Rotting.stl", "Vertically_Challenged.stl", "When_Snow_Falls.stl", "Winter_Wonderland.stl"]]

# TODO Set all these paths to match the locations on your computer.
# "exec" refers to the executable, "data" refers to the data directory.
# Better use the full paths.
exec051 = "/home/ok1asw/tux/supertux-0.5.1/build/supertux2"
data051 = "/home/ok1asw/tux/supertux-0.5.1/data"
exec063 = "/home/ok1asw/tux/supertux/build/supertux2"
data063 = "/home/ok1asw/tux/supertux/data"

tuxes = {
	"004": {"exec":"/home/ok1asw/tux/supertux-0.0.4a/supertux",
	        "level":"/home/ok1asw/tux/supertux-0.0.4a/data/levels/level1.dat",
	        "level_bank":"/home/ok1asw/tux/supertux-0.0.4a/data/level_bank",
	        "lvls":l004,
	        "name":"0.0.4"},
	"014": {"exec":"/home/ok1asw/tux/supertux-0.1.4/src/supertux-milestone1",
	        "data":"/home/ok1asw/tux/supertux-0.1.4/data",
	        "lvls":world1,
	        "name":"0.1.4"},
	"extra": {"exec":"/home/ok1asw/tux/stex/supertux",
	          "data":"/home/ok1asw/tux/stex/data",
	          "lvls":narre,
	        "name":"0.1.4 - Extra"},
	"teratux": {"exec":"/home/ok1asw/tux/teratux/supertux",
	            "data":"/home/ok1asw/tux/teratux/data",
	            "lvls":teratux,
	        "name":"0.1.4 - TeraTux"},
	"051": {"exec":exec051,
	        "data":data051,
	        "lvls":w1_051,
	        "name":"0.5.1 - IcyIsland"},
	"051f": {"exec":exec051,
	         "data":data051,
	         "lvls":w2_051,
	         "name":"0.5.1 - ForestIsland"},
	"051m": {"exec":exec051,
	         "data":data051,
	         "lvls":mattie,
	         "name":"0.5.1 - Mattie's Iceberg"},
	"051y": {"exec":exec051,
	         "data":data051,
	         "lvls":yeti,
	         "name":"0.5.1 - Yeti's Revenge"},
	"051o": {"exec":exec051,
	         "data":data051,
	         "lvls":oug,
	         "name":"0.5.1 - Overunderground"},
	"063": {"exec":exec063,
	        "data":data063,
	        "lvls":w1_063,
	        "name":"0.6.3 - IcyIsland"},
	"063f": {"exec":exec063,
	         "data":data063,
	         "lvls":w2_063,
	         "name":"0.6.3 - ForestIsland"},
	"063c": {"exec":exec063,
	         "data":data063,
	         "lvls":tcc,
	         "name":"0.6.3 - The Crystal Catacombs"},
	"063t": {"exec":exec063,
	         "data":data063,
	         "lvls":treasure,
	         "name":"0.6.3 - Treasure Mountain"},
}

# TODO Fill in your nick and profile photo:
nick = "PINGU"
img  = "https://cdn.discordapp.com/attachments/980565299822739507/993177715089870998/pingu.png"

# The server to login to
host = "37.46.208.34:30001"
