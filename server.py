from http.server import BaseHTTPRequestHandler, HTTPServer
from config import *
import random

tux_battery = [*tuxes.keys()]
random.shuffle(tux_battery)
ncomp = 6
nlevels = 5
seed = 7350
timeout = 300
players = {}
comps = [""]*ncomp
homes = ["-"]*ncomp
tuxtypes = [tux_battery[i % len(tux_battery)] for i in range(0, ncomp)]
random.shuffle(tuxtypes)
winners = []

import time

hostName = "37.46.208.34"
serverPort = 30001

for k in tuxes.keys():
	tuxes[k]["lvls"] = random.sample(tuxes[k]["lvls"], nlevels)

class MyServer(BaseHTTPRequestHandler):
	def do_GET(self):
		#self.wfile.write(bytes("<html><head><title>https://pythonbasics.org</title></head>", "utf-8"))
		self.send_response(500)
		self.send_header("Content-type", "text/html")
		self.end_headers()
		elems = self.path.split("?")
		elems = elems + ["", ""]
		if elems[0] == "/login":
			args = elems[1].split(";")
			if len(args) != 2:
				self.wfile.write(bytes("NOOT NOOT", "utf-8"))
				return
			[name, img] = args
			if name == "PINGU":
				self.wfile.write(bytes("NOOT NOOT", "utf-8"))
				return
			if name not in players.keys():
				if len(players) >= ncomp:
					self.wfile.write(bytes("NOOT NOOT", "utf-8"))
					return
				comp = (len(players)%ncomp) * 2
				if comp >= ncomp:
					comp = comp-ncomp+1
				while comps[comp] != "":
					comp = (comp + 1) % ncomp
				comps[comp] = name
				if homes[comp] == "-":
					homes[comp] = name
				else:
					homes[comp] = homes[comp] + ", " + name
				players[name] = {"img":img, "lvl":0, "comp":comp, "home":comp}
			lvl = players[name]["lvl"]
			if lvl >= nlevels:
				self.wfile.write(bytes("NOOT NOOT", "utf-8"))
				return
			comp = players[name]["comp"]
			tux = tuxtypes[comp]
			self.wfile.write(bytes("%f %d %d %s %s"%(timeout, lvl, comp, tux, tuxes[tux]["lvls"][lvl]), "utf-8"))
		elif elems[0] == "/leveldone":
			args = elems[1].split(";")
			if len(args) != 1:
				self.wfile.write(bytes("NOOT NOOT", "utf-8"))
				return
			name = args[0]
			if name not in players.keys():
				self.wfile.write(bytes("NOOT NOOT", "utf-8"))
				return
			if players[name]["lvl"] >= nlevels:
				self.wfile.write(bytes("NOOT NOOT", "utf-8"))
				return
			comp = players[name]["comp"]
			lvl = players[name]["lvl"]
			comps[comp] = ""
			while True:
				comp = comp + 1
				if comp >= ncomp:
					comp = 0
				if comp == players[name]["home"]:
					lvl = lvl + 1
				if comps[comp] == "":
					break
			players[name]["comp"] = comp
			players[name]["lvl"] = lvl
			if lvl >= nlevels:
				winners.append(name)
				self.wfile.write(bytes("win %s"%(len(winners)), "utf-8"))
				return
			comps[comp] = name
			tux = tuxtypes[comp]
			self.wfile.write(bytes("%d %d %s %s"%(lvl, comp, tux, tuxes[tux]["lvls"][lvl]), "utf-8"))
		else:
			self.wfile.write(bytes("<html><head><meta charset=\"UTF-8\"></head><body><table border=1>\n", "utf-8"))
			for i in range(0, ncomp):
				self.wfile.write(bytes("<tr><td><center><h1>P%d</h1><br>%s<br>%s</center></td>\n"%
					(i+1, tuxes[tuxtypes[i]]["name"], homes[i]), "utf-8"))
				if comps[i] == "":
					self.wfile.write(bytes("<td></td><td></td></tr>\n"%(), "utf-8"))
				else:
					self.wfile.write(bytes("<td><center><img src=\"%s\" width=128px height=128px><br>%s</center></td>\n"%
						(players[comps[i]]["img"], comps[i]), "utf-8"))
					self.wfile.write(bytes("<td><center>level<br><h1>%d</h1></center></td></tr>\n"%
						(players[comps[i]]["lvl"]+1), "utf-8"))
			self.wfile.write(bytes("</table>", "utf-8"))
			self.wfile.write(bytes("<script>\nif (document.referrer !== document.location.href) {\nsetTimeout(function() {\n    document.location.reload()\n}, 1000);\n}</script>", "utf-8"))
			if len(winners):
				self.wfile.write(bytes("<h1>winners:</h1>", "utf-8"))
				self.wfile.write(bytes("<table><tr>", "utf-8"))
				for i, name in enumerate(winners):
					self.wfile.write(bytes("<td><center><img src=\"%s\" width=128px height=128px><br><b>%d:</b> %s</center></td>"%(players[name]["img"], i+1, name), "utf-8"))
				self.wfile.write(bytes("</tr></table>", "utf-8"))
			self.wfile.write(bytes("</body></html>", "utf-8"))

if __name__ == "__main__":        
    webServer = HTTPServer((hostName, serverPort), MyServer)
    print("Server started http://%s:%s" % (hostName, serverPort))

    try:
        webServer.serve_forever()
    except KeyboardInterrupt:
        pass

    webServer.server_close()
    print("Server stopped.")
