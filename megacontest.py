import os
from config import *
import subprocess
from colorama import Fore
from threading import Thread
import time
from inputimeout import inputimeout, TimeoutOccurred
import requests
import shutil

nlevels = 5

process = None
timeout = False
legit = False

res = requests.get("http://"+host+"/login?%s;%s"%(nick, img))
res = res.text.split(" ")
max_time = float(res[0])
lvl = int(res[1])+1
comp = int(res[2])+1
tux = res[3]
lvln = " ".join(res[4:])

def exec_level_thread(tux, lvl, lvli):
	global process
	global timeout
	global legit
	try:
		inputimeout(Fore.CYAN + "Running level %d of SuperTux %s, press"%(lvli, tux) + Fore.GREEN +" ENTER."+ Fore.WHITE, 3)
	except TimeoutOccurred:
		pass
	if timeout:
		return
	cont = True
	while cont:
		if "data" in tuxes[tux].keys():
			process = subprocess.Popen([tuxes[tux]["exec"], os.path.join(tuxes[tux]["data"], "levels", lvl)])
		else:
			my_dir = os.getcwd()
			os.chdir(os.path.dirname(tuxes[tux]["exec"]))
			shutil.copy(os.path.join(tuxes[tux]["level_bank"], lvl), tuxes[tux]["level"])
			process = subprocess.Popen(tuxes[tux]["exec"])
			os.chdir(my_dir)
		process.wait()
		if process == None:
			return
		process = None
		while True:
			if timeout:
				return
			try:
				s = inputimeout(Fore.YELLOW + "Did you finish the level?" + Fore.GREEN + " yes/no "+ Fore.WHITE, 3).lower()
				if s == "yes":
					cont = False
					legit = True
					break
				elif s == "no":
					break
			except TimeoutOccurred:
				continue

def exec_level(tux, lvl, lvli):
	global process
	global timeout
	global legit
	legit = False
	timeout = False
	while True:
		t = Thread(target = exec_level_thread, args = (tux, lvl, lvli))
		t.start()
		t.join(max_time)
		if legit or t.is_alive():
			break
		else:
			print("Thread exited violently, restarting...")
	if t.is_alive():
		timeout = True
		if process != None:
			process.kill()
			process = None
		t.join()
		print(Fore.CYAN + "Time's up! Go to the next computer." + Fore.WHITE)

while True:
	print(Fore.CYAN + "Playing level %d at the computer P%d"%(lvl, comp) + Fore.WHITE)
	exec_level(tux, lvln, lvl)
	res = requests.get("http://"+host+"/leveldone?%s"%(nick))
	res = res.text.split(" ")
	if len(res) == 2:
		print(Fore.CYAN + "Congratulations! You won! Your position is: %s"%(res[1]) + Fore.WHITE)
		break
	lvl = int(res[0])+1
	comp = int(res[1])+1
	tux = res[2]
	lvln = " ".join(res[3:])
